from django.db import models
from django.urls import reverse


class City(models.Model):
    name = models.CharField('Город', max_length=100, unique=True)

    class Meta:
        verbose_name = 'Город'
        verbose_name_plural = 'Города'
        ordering = ('name', )

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('cities:city-detail', kwargs={'pk': self.pk})
