from django.urls import path

from .views import *


urlpatterns = [
    path('', home, name='home'),
    path('add/', CityCreateView.as_view(), name='city-create'),
    path('detail/<int:pk>/', CityDetailView.as_view(), name='city-detail'),
    path('update/<int:pk>/', CityUpdateView.as_view(), name='city-update'),
    path('delete/<int:pk>/', CityDeleteView.as_view(), name='city-delete'),
]
